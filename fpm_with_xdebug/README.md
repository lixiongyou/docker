# 说明

php-fpm 带有配置和 xdebug 的镜像

## 1. 下载镜像：docker pull lixiongyou/fpm_with_xdebug

## 2. 模块说明：
* php-fpm 监听容器端口：9001，自动启动
* xdebug 监听容器端口：9000
