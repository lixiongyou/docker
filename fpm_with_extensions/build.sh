#!/bin/sh

current_dir="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 || exit ; pwd -P )"
current_dir_name=$(basename "$current_dir")

docker buildx build -t lixiongyou/"$current_dir_name":latest --push --platform=linux/arm64,linux/amd64 "$current_dir"
