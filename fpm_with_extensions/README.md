# 说明

fpm alpine 镜像

## 1. 下载镜像：docker pull lixiongyou/fpm_with_extensions

## 2. 已装扩展：
 * apcu
 * yac
 * xdebug
 * mbstring 
 * sockets 
 * bcmath 
 * bz2 
 * calendar
 * exif
 * gettext
 * http 
 * igbinary 
 * mcrypt 
 * msgpack 
 * mysqli 
 * pcntl 
 * pdo_mysql 
 * redis 
 * sysvmsg 
 * sysvsem 
 * sysvshm 
 * zip 
 * gmp 
 * memcache 
 * memcached 
 * shmop
