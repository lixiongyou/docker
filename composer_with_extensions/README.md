# 说明

## 1. 下载镜像：docker pull lixiongyou/composer_with_extensions

## 2. 使用说明：[composer docker](https://hub.docker.com/_/composer)

## 3. 举例：

composer update：

docker run --rm -it -v "$PWD:/app" -v "$HOME/.ssh:/root/.ssh" lixiongyou/composer_with_extensions composer update
